package com.jstorm.audiologger;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.util.Log;

import static android.content.ContentValues.TAG;

class SettingsContentObserver extends ContentObserver {

    private Context context;
    private int previousVolume;
    Storage storage;

    SettingsContentObserver(Handler handler, Context c) {
        super(handler);
        context = c;
        storage = new Storage(c);

        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        previousVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
    }

    @Override
    public boolean deliverSelfNotifications() {
        return super.deliverSelfNotifications();
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);

        Log.d(TAG, "Settings change detected: "+currentVolume);
        if(currentVolume < previousVolume)
        {
            Log.d(TAG,"Decreased");
            storage.saveVolume(currentVolume,"Decreased");
        }
        else if(currentVolume > previousVolume)
        {
            Log.d(TAG,"Increased");
            storage.saveVolume(currentVolume,"Increased");
        }
        previousVolume=currentVolume;
    }
}