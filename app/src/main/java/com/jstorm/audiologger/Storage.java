package com.jstorm.audiologger;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import static android.content.ContentValues.TAG;

/**
 * Created by Jan Storm on 30.10.2017.
 */

class Storage {
    private Context context;
    private String filename;

    Storage(Context c){
        context = c;
        String externalDirPath = "/storage/emulated/0/";
        filename = externalDirPath + "/" + "AudioLog.log";
    }

    boolean save(String content) {
        try {
            FileWriter fw = new FileWriter(filename, true);
            fw.write(content + "\n");
            fw.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    boolean saveVolume(int volume, String additionalText){
        Calendar cal = new GregorianCalendar();
        Date date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm:ss", Locale.GERMANY);
        String line = "["+sdf.format(date)+"] Volume: "+volume;
        if(additionalText != "")
            line += ", extra:  "+additionalText;

        return save(line);
    }
}
